#include "ChineseFrequency.h"
#include <stdexcept>
#include <muduo/base/Logging.h>
using namespace std;
using namespace CppJieba;

ChineseFrequency::ChineseFrequency(std::string ChFilename,
		   			 			   std::string ChStopfile,
		   			 			   std::string ChDictname,
		   			 			   std::string jiebaDictPath,
		   			 			   std::string jiebaModelPath)
	:_ChFilename(std::move(ChFilename)),
	 _ChStopfile(std::move(ChStopfile)),
	 _ChDictname(std::move(ChDictname)),
	 _jiebaDictPath(std::move(jiebaDictPath)),
	 _jiebaModelPath(std::move(jiebaModelPath)),
	 _segment(_jiebaDictPath, _jiebaModelPath)
{
	LOG_INFO << "jieba自动分词!!";
}

void ChineseFrequency::readStopList()
{
	ifstream in(_ChStopfile);
	if(!in)
		throw runtime_error("stopList打开失败!!");

	string word;
	while(in >> word)
		_stopList.insert(word);

	if(!in.eof())
		throw runtime_error("error ch stoplist file");

	in.close();
}


void ChineseFrequency::readWordFile()
{
	ifstream in(_ChFilename);
	if(!in)
		throw runtime_error("word文件打开失败!!");

	_words.clear();
	string line;
	vector<string> result;		//存放分词结果
	while(getline(in, line))
	{
		result.clear();
		_segment.cut(line, result);
		for(vector<string>::const_iterator it = result.begin();
			it != result.end();
			++ it)
		{
			if(_stopList.count(*it) == 0)
				++ _words[*it];
		}
	}

	if(!in.eof())
		throw runtime_error("error ch file");

	in.close();
}


void ChineseFrequency::saveDict() const
{
	ofstream out(_ChDictname);
	if(!out)
		throw runtime_error("Dict打开失败!!");

	for(auto it = _words.begin(); it != _words.end(); ++ it)
		out << it->first << " " << it->second << endl;

	out.close();
}