#ifndef CHINESE_FREQUENCY_H
#define CHINESE_FREQUENCY_H 



#include <unordered_map>
#include <unordered_set>
#include <string>
#include <vector>
#include <boost/noncopyable.hpp>
#include "../include/libjieba/MixSegment.hpp"

class ChineseFrequency : boost::noncopyable
{
public:
	ChineseFrequency(std::string ChFilename,
		   			 std::string ChStopfile,
		   			 std::string ChDictname,
		   			 std::string jiebaDictPath,
		   			 std::string jiebaModelPath);

	void readStopList();
	void readWordFile();
	void saveDict() const;
private:
	std::string _ChFilename;
	std::string _ChStopfile;
	std::string _ChDictname;

	std::string _jiebaDictPath;
	std::string _jiebaModelPath;

	CppJieba::MixSegment _segment;

	std::unordered_map<std::string, int> _words;
	std::unordered_set<std::string> _stopList;


};

#endif