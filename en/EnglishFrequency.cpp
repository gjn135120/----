#include "EnglishFrequency.h"
#include <fstream>
#include "StringUtil.h"
#include <stdexcept>
#include <algorithm>


using namespace std;
using namespace StringUtil;

EnglishFrequency::EnglishFrequency(std::string filename, 
					   			   std::string stopfile, 
					   			   std::string dictname)
	:_filename(std::move(filename)),
	 _stopfile(std::move(stopfile)),
	 _dictname(std::move(dictname))
{

}

//从语料中读取单词
void EnglishFrequency::readWordFile()
{
	ifstream in(_filename.c_str());
	if(!in)
		throw runtime_error("语料打开失败!!");

	_words.clear();
	string word;
	while(in >> word)
	{
		erasePunct(word);
		if(isAllDigit(word))
			continue;

		stringToLower(word);

		if(_stopList.count(word) == 0)
			++ _words[word];
	}


	in.close();
}

//读取停用词集合
void EnglishFrequency::readStopList()
{
	ifstream in(_stopfile.c_str());
	if(!in)
		throw runtime_error("stopList打开失败!!");

	string word;
	while(in >> word)
		_stopList.insert(word);

	in.close();

}

//将词典保存
void EnglishFrequency::saveDict()
{
	ofstream out(_dictname.c_str());
	if(!out)
		throw runtime_error("dict打开失败!!");

	for(auto it = _words.begin();
		it != _words.end();
		++ it)
		out << it->first << " " << it->second << endl;

	out.close();
}