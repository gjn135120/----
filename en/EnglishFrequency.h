#ifndef WORD_FREQUENCY_H
#define WORD_FREQUENCY_H 


#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <boost/noncopyable.hpp>

class EnglishFrequency : boost::noncopyable
{
public:

	EnglishFrequency(std::string filename, 
					 std::string stopfile, 
					 std::string dictname);

	void readWordFile();
	void readStopList();
	void saveDict();

private:

	std::string _filename;		//语料
	std::string _stopfile;		//停用词文件
	std::string _dictname;		//保存字典文件


	std::unordered_map<std::string, int> _words;	//单词及相应词频集合
	std::unordered_set<std::string> _stopList;			//停用词集合

};

#endif	//WORD_FREQUENCY_H