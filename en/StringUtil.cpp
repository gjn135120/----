#include "StringUtil.h"
#include <ctype.h>

using namespace std;

namespace StringUtil
{

//删除单词中的字符
void erasePunct(std::string &word)
{
	string::iterator it = word.begin();

	while(it != word.end())
	{
		if(ispunct(*it))
			it = word.erase(it);
		else
			++ it;
	}
}

//将单词变为小写
void stringToLower(std::string &word)
{
	string::iterator it = word.begin();

	for( ; it != word.end(); ++ it)
	{
		if(isupper(*it))
			*it = tolower(*it);
	}
}

//判断单词是否全是数字
bool isAllDigit(const std::string &word)
{
	for(string::const_iterator it = word.begin();
		it != word.end();
		++ it)
	{
		if(!isdigit(*it))
			return false;
	}

	return true;
}


}