#ifndef STRING_UTIL_H
#define STRING_UTIL_H 

#include <string>

namespace StringUtil
{

void erasePunct(std::string &word);
void stringToLower(std::string &word);
bool isAllDigit(const std::string &word);

}


#endif	//STRING_UTIL_H