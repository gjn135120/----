#include "InvertedIndex.h"
#include "StringUtil.h"
using namespace std;

void InvertedIndex::addItem(pair<string, int> item)
{
	vector<uint32_t> vec;
	StringUtil::parseUTF8String(item.first, vec);
	for(vector<uint32_t>::iterator it = vec.begin();
		it != vec.end();
		++ it)
		_index[*it].insert(std::move(item));
}


set<pair<string, int> > InvertedIndex::getWords(const string &s) const
{
	set<pair<string, int> > result;
	vector<uint32_t> vec;
	StringUtil::parseUTF8String(s, vec);

	for(vector<uint32_t>::iterator it = vec.begin();
		it != vec.end();
		++ it)
	{
		InvertedIndex::INDEX_MAP::const_iterator tmp = _index.find(*it);
		if(tmp != _index.end())
			result.insert(tmp->second.begin(), tmp->second.end());
	}

	return result;
}