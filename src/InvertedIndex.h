#ifndef INVERTED_INDEX_H
#define INVERTED_INDEX_H 

#include <boost/noncopyable.hpp>
#include <unordered_map>
#include <set>
#include <string>
#include <stdint.h>


class InvertedIndex : boost::noncopyable
{
public:

	void addItem(std::pair<std::string, int> item);
	std::set<std::pair<std::string, int> > getWords(const std::string &s) const;

private:

	typedef std::unordered_map<uint32_t, std::set<std::pair
							  <std::string, int> > > INDEX_MAP;

	INDEX_MAP _index;
};

#endif