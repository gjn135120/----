#include "QueryServer.h"
#include <muduo/base/Logging.h>
using namespace muduo;
using namespace muduo::net;
using namespace std::placeholders;

QueryServer::QueryServer(const std::string &EnDict,
						 const std::string &ChDict,
						 const std::string &redisHost,
						 uint16_t port,
						 muduo::net::EventLoop *loop,
						 const muduo::net::InetAddress &addr)
	:_query(EnDict, ChDict, redisHost, port),
	 _server(loop, addr, "QueryServer")
{
	_server.setMessageCallback(std::bind(&QueryServer::onMessage, this, _1, _2, _3));

}

void QueryServer::onMessage(const TcpConnectionPtr &conn, Buffer *buf, Timestamp t)
{
	string s(buf->retrieveAllAsString());
	LOG_INFO << "receive php msg : " << s;
	std::string result = _query.queryWord(s.c_str());
	conn->send(result.c_str(), result.size());
}