#include "RedisClient.h"
#include <string.h>
#include <muduo/base/Logging.h>
#include <memory>
using namespace muduo;

namespace
{

struct Delete
{
public:
	void operator() (redisReply *reply) const
	{
		freeReplyObject(reply);
	} 
};

}


RedisClient::RedisClient(const std::string &host, uint16_t port = 6379)
{
	_connect = redisConnect(host.c_str(), port);
	if(_connect == NULL || _connect->err)
		LOG_FATAL << "redis connect host : " << host << " port : " << port;

	else
		LOG_INFO << "redis connect successed!!";
}

RedisClient::~RedisClient()
{
	redisFree(_connect);
	LOG_INFO << "redis connect CLOSED!!";
}


void RedisClient::setKeyValue(const std::string &key, const std::string &value) const
{
	LOG_DEBUG << "redis setKeyValue key : " << key << " value : " << value;

	std::unique_ptr<redisReply, Delete> ret
			(static_cast<redisReply *>
				(redisCommand(_connect, "set %s %s", key.c_str(), value.c_str())),
			Delete());

	if(ret->type == REDIS_REPLY_ERROR)
		LOG_ERROR << "redis setKeyValue ERROR key : " << key << " value : " <<value;

}

std::pair<std::string, bool> RedisClient::getValueByKey(const std::string &key) const
{
	LOG_DEBUG << "redis getValueByKey key : " << key;

	std::unique_ptr<redisReply, Delete> ret
			(static_cast<redisReply *>
				(redisCommand(_connect, "get %s", key.c_str())),
			Delete());

	if(ret->type == REDIS_REPLY_STRING)
	{
		LOG_DEBUG << "redis getValueByKey key : " << key << " value : " << ret->str;
		return make_pair(std::string(ret->str), true);
	}

	else if(ret->type == REDIS_REPLY_NIL)
	{
		LOG_DEBUG << "redis getValueByKey key : " <<key << "key isn't exist!!";
		return make_pair(std::string(""), false);
	}

	else if(ret->type == REDIS_REPLY_ERROR)
		LOG_FATAL << "redis command replied with an error!!";
	else
		LOG_FATAL << "redis command replied with an unknown result!!";

	return make_pair(std::string(""), false);
}
