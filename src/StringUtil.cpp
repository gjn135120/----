#include "StringUtil.h"
#include <ctype.h>
#include <assert.h>
#include <string.h>

using namespace std;

namespace
{
inline int Min(int a, int b, int c)
{
	int ret = a < b ? a : b;
	ret = ret < c ? ret : c;

	return ret;
}

//计算UTF8编码所占的字节
int getLenOfUTF8(unsigned char c)
{
	int cnt = 0;
	while(c & (1 << (7 - cnt)))
		++ cnt;

	return cnt;
}

}

namespace StringUtil
{

//删除单词中的字符
void erasePunct(std::string &word)
{
	string::iterator it = word.begin();

	while(it != word.end())
	{
		if(ispunct(*it))
			it = word.erase(it);
		else
			++ it;
	}
}

//将单词变为小写
void stringToLower(std::string &word)
{
	string::iterator it = word.begin();

	for( ; it != word.end(); ++ it)
	{
		if(isupper(*it))
			*it = tolower(*it);
	}
}

//判断单词是否全是数字
bool isAllDigit(const std::string &word)
{
	for(string::const_iterator it = word.begin();
		it != word.end();
		++ it)
	{
		if(!isdigit(*it))
			return false;
	}

	return true;
}

//计算英文编辑距离
int editDistanceStr(const std::string &a, const std::string &b)
{
	assert(a.size() < 100 && b.size() < 100);
	int arr[100][100];
	memset(arr, 0, sizeof arr);

	for(size_t i = 0; i <= a.size(); ++ i)
		arr[i][0] = i;

	for(size_t j = 0; j <= b.size(); ++ j)
		arr[0][j] = j;

	for(size_t i = 1; i <= a.size(); ++ i)
	{
		for(size_t j = 1; j <= b.size(); ++ j)
		{
			if(a[i - 1] == b[j - 1])
				arr[i][j] = arr[i - 1][j - 1];
			else
			{
				int t1 = arr[i - 1][j - 1];
				int t2 = arr[i - 1][j];
				int t3 = arr[i][j - 1];
				arr[i][j] = Min(t1, t2, t3) + 1;
			}
		}
	}

	return arr[a.size()][b.size()];

}

void parseUTF8String(const string &s, vector<uint32_t> &vec)
{
	vec.clear();
	for(string::size_type i = 0; i != s.size(); ++ i)
	{
		assert(i < s.size());
		int len = getLenOfUTF8(s[i]);
		uint32_t t = (unsigned char)s[i];
		if(len > 1)
		{
			-- len;
			while(len --)
				t = (t << 8) | s[++i];
		}
		vec.push_back(t);
	}
}

int editDistanceUint32(const vector<uint32_t> &a, const vector<uint32_t> &b)
{
	int len1 = a.size();
	int len2 = b.size();
	int arr[100][100];
	memset(arr, 0, sizeof arr);
	for(int i = 0; i <= len1; ++ i)
		arr[i][0] = i;
	for(int j = 0; j <= len2; ++ j)
		arr[0][j] = j;

	for(int i = 1; i<= len1; ++ i)
	{
		for(int j = 1; j <= len2; ++ j)
		{
			if(a[i - 1] == b[j - 1])
				arr[i][j] = arr[i - 1][j - 1];
			else
			{
				int t1 = arr[i - 1][j - 1];
				int t2 = arr[i - 1][j];
				int t3 = arr[i][j - 1];
				arr[i][j] = Min(t1, t2, t3) + 1;
			}
		}
	}

	return arr[len1][len2];
}

int editDistance(const string &a, const string &b)
{
	vector<uint32_t> w1, w2;
	parseUTF8String(a, w1);
	parseUTF8String(b, w2);
	return editDistanceUint32(w1, w2);
}


bool isASCIIString(const std::string &word)
{
	for(size_t i = 0; i <= word.size(); ++ i)
	{
		if(isascii(word[i]))
			return false;
	}

	return true;
}

}