#ifndef STRING_UTIL_H
#define STRING_UTIL_H 

#include <string>
#include <vector>
#include <stdint.h>

namespace StringUtil
{

void erasePunct(std::string &word);
void stringToLower(std::string &word);
bool isAllDigit(const std::string &word);
int editDistanceStr(const std::string &a, const std::string &b);
int editDistance(const std::string &a, const std::string &b);
void parseUTF8String(const std::string &s, std::vector<uint32_t> &vec);
bool isASCIIString(const std::string &word);

}


#endif	//STRING_UTIL_H