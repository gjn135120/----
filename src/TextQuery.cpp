#include "TextQuery.h"
#include "StringUtil.h"
#include <fstream>
#include <functional>
#include <vector>
#include <queue>
#include <muduo/base/Logging.h>
#include <limits>

using namespace std;
using namespace muduo;
using namespace std::placeholders;

namespace
{
struct Word
{
	Word(std::string word, int dis, int frequency)
		:_word(std::move(word)),
		 _distance(dis),
		 _frequency(frequency)
	{

	}

	std::string _word;
	int _distance;
	int _frequency;
};

bool operator< (const Word &a, const Word &b)
{
	if(a._distance != b._distance)
		return a._distance > b._distance;
	return a._frequency < b._frequency;
}

}

TextQuery::TextQuery(std::string EnDictname, std::string ChDictname,
					 const std::string &host, uint16_t port)
	:_EnDictname(std::move(EnDictname)),
	 _ChDictname(std::move(ChDictname)),
	 _client(host, port)
{
	readEnDict();
	readChDict();
}

std::string TextQuery::queryWord(const std::string &word) const
{
	std::pair<std::string, bool> ret = _client.getValueByKey(word);
	if(ret.second)
		return ret.first;
	else
	{
		std::string s = queryWordInDict(word);
		_client.setKeyValue(word, s);
		return s;
	}
}

std::string TextQuery::queryWordInDict(const std::string &word) const
{
	priority_queue<Word, vector<Word>, less<Word> > que;

	LOG_DEBUG << "query word : " << word;

	std::set<std::pair<std::string, int> > words = _index.getWords(word);

	bool flag = StringUtil::isASCIIString(word);
	for(std::set<std::pair<std::string, int> >::iterator it = words.begin();
		it != words.end();
		++ it)
	{
		int dis;
		if(flag)
			dis = StringUtil::editDistanceStr(word, it->first);
		else
		 	dis = StringUtil::editDistance(word, it->first);
		if((static_cast<double>(dis) / word.size()) < 0.5 )
			que.push(Word(it->first, dis, it->second));
	}
	if(!que.empty())
		return que.top()._word;
	else
		return std::string();
}


void TextQuery::readEnDict()
{
	std::ifstream in(_EnDictname);

	if(!in)
		LOG_FATAL << "open En file error!!";

	LOG_INFO << "Begin read En Dict : " << _EnDictname;

	std::string word;
	int cnt;
	while(in >> word >> cnt)
	{
		if(in.fail())
		{
			LOG_WARN << "En file format error in : " << _EnDictname;
			in.clear();
			in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		_index.addItem(make_pair(word, cnt));
	}


	LOG_INFO << "read En Dict Finish";

	in.close();

}

void TextQuery::readChDict()
{
	ifstream in(_ChDictname);
	if(!in)
		LOG_FATAL << "open Ch file error!!";

	LOG_INFO << "Begin read Ch Dict : " << _ChDictname;

	std::string word;
	int cnt;
	while(in >> word >> cnt, !in.eof())
	{
		if(in.fail())
		{
			LOG_WARN << "Ch file format error in : " << _ChDictname;
			in.clear();
			in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		_index.addItem(make_pair(word, cnt));
	}

	LOG_INFO << "read in ChDict Finish";
	in.close();
}