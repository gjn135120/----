#ifndef TEXT_QUERY_H
#define TEXT_QUERY_H 


#include <boost/noncopyable.hpp>
#include <unordered_map>
#include <string>
#include "RedisClient.h"
#include "InvertedIndex.h"

class TextQuery : boost::noncopyable
{
public:

	TextQuery(std::string EnDictname, std::string ChDictname, 
			  const std::string &host, uint16_t port);
	std::string queryWord(const std::string &word) const;

private:

	void readEnDict();		//读取英文词典
	void readChDict();		//读取中文词典

	std::string queryWordInDict(const std::string &word) const;

	std::string _EnDictname;						//英文词典文件
	std::string _ChDictname;						//中文词典文件
	//std::unordered_map<std::string, int> _dict;		//保存词典的集合
	InvertedIndex _index;			//倒排索引
	RedisClient _client;
};

#endif	//TEXT_QUERY_H