#include "RedisClient.h"
#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	RedisClient client("localhost", 6397);
	string key;
	string value;
	while(cin >> key >> value)
		client.setKeyValue(key, value);

	cin.clear();

	while(cin >> key)
	{
		pair<string, bool> ret = client.getValueByKey(key);
		if(ret.second)
			cout << "key : " << key << " value : " << value << endl;
		else
			cout << "key : " << key << " value isn't exist" << endl;
	}
	return 0;
}